# noetic-cs-tp
 Dockerfile for TP at CS

```bash
docker build --build-arg usr="<username>" --build-arg pwd="<PAT>" --progress=plain -t noetic-cs .
docker run -it --net=ros --name=noetic-cs --env="DISPLAY=novnc:0.0" --env="ROS_MASTER_URI=http://localhost:11311" noetic-cs /bin/bash
docker exec -it --env="DISPLAY=novnc:0.0" --env="ROS_MASTER_URI=http://localhost:11311" noetic-cs bash
```
NOTE: PAT is not stored anywhere during the process. it is discarded immediately after the use.
