FROM ros:noetic-ros-base

ARG usr
ARG pwd

RUN rm -rf /var/lib/apt/lists/* && apt-get update && apt-get upgrade -y ;\
    apt-get install -y git bash wget coreutils; \
    mkdir -p /home/tp_cs/catkin_ws/src

COPY share/ /home/tp_cs
ENV HOME=/home/tp_cs \
    DISPLAY_WIDTH=1920 \
    DISPLAY_HEIGHT=1080 
WORKDIR /home/tp_cs

RUN apt update ;\
    sed -i '15s/^/force_color_prompt=yes\n/' /root/.bashrc;\
    apt install --no-install-recommends -y python3-rosinstall python3-rosinstall-generator python3-wstool python3-catkin-tools;\
    apt install -y ros-noetic-rviz ros-noetic-gazebo-ros-* ros-noetic-xacro ros-noetic-robot-state-publisher ros-noetic-tf*;\ 
    apt update && apt upgrade -y;\
    mkdir /root/.ssh && touch /root/.ssh/known_hosts && ssh-keygen -F gitlab-student.centralesupelec.fr || ssh-keyscan gitlab-student.centralesupelec.fr >> /root/.ssh/known_hosts;\
    cd /home/tp_cs && sed -i -e 's/\r$//' sim_launch.sh && chmod +x sim_launch.sh && python3 setup.pyc --usr ${usr} --pwd ${pwd};\
    chmod +x /home/tp_cs/catkin_ws/src/heterogeneous_mrc/scripts/control_lib.pyc /home/tp_cs/catkin_ws/src/heterogeneous_mrc/scripts/run.sh /home/tp_cs/catkin_ws/src/heterogeneous_mrc/scripts/runtime.py

    #sed -i '6d' /ros_entrypoint.sh
RUN cd /home/tp_cs/catkin_ws/ ;\
    /bin/bash -c "source /opt/ros/noetic/setup.bash && catkin build" ;\
    echo "source /opt/ros/noetic/setup.bash" >> /root/.bashrc ;\
    echo "source /home/tp_cs/catkin_ws/devel/setup.bash" >> /root/.bashrc

CMD [ "/bin/bash" ]

