#!/bin/bash
set -e

. /root/.bashrc
. /opt/ros/noetic/setup.bash
cd catkin_ws && . devel/setup.bash
roslaunch heterogeneous_mrc exp_all.launch num_of_tb3:=$1 tb3_1_pos_x:=$2 tb3_1_pos_y:=$3 tb3_1_yaw:=$4 tb3_2_pos_x:=$5 tb3_2_pos_y:=$6 tb3_2_yaw:=$7 tb3_3_pos_x:=$8 tb3_3_pos_y:=$9 tb3_3_yaw:=${10} tb3_4_pos_x:=${11} tb3_4_pos_y:=${12} tb3_4_yaw:=${13} tb3_5_pos_x:=${14} tb3_5_pos_y:=${15} tb3_5_yaw:=${16}
